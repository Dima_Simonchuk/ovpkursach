﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ovpCursach
{
    class BuildTables
    {
        public static void BuildMatrixC(DataGridView data)
        {
            data.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            data.AllowUserToAddRows = false;
            data.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            data.Rows.Add(0, 3, 0, 6, 1, 0, 6);
            data.Rows.Add(3, 0, 4, 0, 2, 3, 0);
            data.Rows.Add(0, 4, 0, 5, 0, 1, 0);
            data.Rows.Add(6, 0, 5, 0, 0, 4, 5);
            data.Rows.Add(1, 2, 0, 0, 0, 2, 2);
            data.Rows.Add(0, 3, 1, 4, 2, 0, 0);
            data.Rows.Add(6, 0, 0, 5, 2, 0, 0);

            for (int i = 0; i < 7; i++)
            {
                if (i < 4) { data.Rows[i].HeaderCell.Value = "A" + (i + 1); }
                else
                { data.Rows[i].HeaderCell.Value = "B" + (i - 3); }
            }
        }

        public static void BuildMatrixShorts(DataGridView data)
        {
            data.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            data.AllowUserToAddRows = false;
            data.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            data.Rows.Add("", "", "", 7);
            data.Rows.Add("", "", "", 8);
            data.Rows.Add("", "", "", 9);
            data.Rows.Add("", "", "", 6);
            data.Rows.Add(12, 8, 10, "");

            for (int i = 0; i < 5; i++)
            {
                if (i < 4) { data.Rows[i].HeaderCell.Value = "A" + (i + 1); }
                else
                { data.Rows[i].HeaderCell.Value = "bj"; }
            }
        }
    }
}
