﻿namespace ovpCursach
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MatrixC = new System.Windows.Forms.DataGridView();
            this.A1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.A2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.A3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.A4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.B1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.B2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.B3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MatrixD = new System.Windows.Forms.DataGridView();
            this.B1s = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.B2s = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.B3s = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ais = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.MatrixC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MatrixD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // MatrixC
            // 
            this.MatrixC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MatrixC.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.A1,
            this.A2,
            this.A3,
            this.A4,
            this.B1,
            this.B2,
            this.B3});
            this.MatrixC.Location = new System.Drawing.Point(12, 121);
            this.MatrixC.Name = "MatrixC";
            this.MatrixC.Size = new System.Drawing.Size(245, 178);
            this.MatrixC.TabIndex = 0;
            this.MatrixC.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // A1
            // 
            this.A1.HeaderText = "A1";
            this.A1.Name = "A1";
            this.A1.Width = 25;
            // 
            // A2
            // 
            this.A2.HeaderText = "A2";
            this.A2.Name = "A2";
            this.A2.Width = 25;
            // 
            // A3
            // 
            this.A3.HeaderText = "A3";
            this.A3.Name = "A3";
            this.A3.Width = 25;
            // 
            // A4
            // 
            this.A4.HeaderText = "A4";
            this.A4.Name = "A4";
            this.A4.Width = 25;
            // 
            // B1
            // 
            this.B1.HeaderText = "B1";
            this.B1.Name = "B1";
            this.B1.Width = 25;
            // 
            // B2
            // 
            this.B2.HeaderText = "B2";
            this.B2.Name = "B2";
            this.B2.Width = 25;
            // 
            // B3
            // 
            this.B3.HeaderText = "B3";
            this.B3.Name = "B3";
            this.B3.Width = 25;
            // 
            // MatrixD
            // 
            this.MatrixD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MatrixD.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.B1s,
            this.B2s,
            this.B3s,
            this.ais});
            this.MatrixD.Location = new System.Drawing.Point(552, 121);
            this.MatrixD.Name = "MatrixD";
            this.MatrixD.Size = new System.Drawing.Size(230, 134);
            this.MatrixD.TabIndex = 1;
            this.MatrixD.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MatrixShort_CellContentClick);
            // 
            // B1s
            // 
            this.B1s.HeaderText = "B1";
            this.B1s.Name = "B1s";
            // 
            // B2s
            // 
            this.B2s.HeaderText = "B2";
            this.B2s.Name = "B2s";
            // 
            // B3s
            // 
            this.B3s.HeaderText = "B3";
            this.B3s.Name = "B3s";
            // 
            // ais
            // 
            this.ais.HeaderText = "ai";
            this.ais.Name = "ais";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(383, 302);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(240, 150);
            this.dataGridView1.TabIndex = 2;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(701, 302);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(240, 150);
            this.dataGridView2.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(953, 464);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.MatrixD);
            this.Controls.Add(this.MatrixC);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.MatrixC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MatrixD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView MatrixC;
        private System.Windows.Forms.DataGridViewTextBoxColumn A1;
        private System.Windows.Forms.DataGridViewTextBoxColumn A2;
        private System.Windows.Forms.DataGridViewTextBoxColumn A3;
        private System.Windows.Forms.DataGridViewTextBoxColumn A4;
        private System.Windows.Forms.DataGridViewTextBoxColumn B1;
        private System.Windows.Forms.DataGridViewTextBoxColumn B2;
        private System.Windows.Forms.DataGridViewTextBoxColumn B3;
        private System.Windows.Forms.DataGridView MatrixD;
        private System.Windows.Forms.DataGridViewTextBoxColumn B1s;
        private System.Windows.Forms.DataGridViewTextBoxColumn B2s;
        private System.Windows.Forms.DataGridViewTextBoxColumn B3s;
        private System.Windows.Forms.DataGridViewTextBoxColumn ais;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
    }
}

