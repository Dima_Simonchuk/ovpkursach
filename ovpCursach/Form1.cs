﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ovpCursach
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            BuildTables.BuildMatrixC(MatrixC);
            BuildTables.BuildMatrixShorts(MatrixD);

            Console.WriteLine(MatrixC.Rows[0].Cells[1].Value);
            Deikstra();

        }

        public void Deikstra()
        {
            int A = 4, B = 3, c, w;
            int[,] D = new int[A + B, A + B]; // минимальное расстояние
            int[,] P = new int[A + B, A + B];
            int[] V = new int[] {0,1,2,3,4,5,6};
            int[] S = new int[7], VnotS = new int[A+B];

            for (int i = 0; i < A; i++)
            {
                S[0] = i;
                for (int j = 0; j < A+B; j++)
                {
                    c = Convert.ToInt32(MatrixC.Rows[i].Cells[j].Value);
                    if (c == 0)
                        c = 1000000;
                    D[i, j] = D[i, j] + c;
                    P[i, j] = P[i, j] + i;
                }
                for (int j = 0; j < A+B-1; j++)
                {
                    VnotS = V.Where(x => x != S[0]).ToArray();
                    w = VnotS[0];
                    for (int x = 0; x < VnotS.Length; x++)
                        if (D[i, VnotS[x]] < D[i, w])
                            w = VnotS[x];
                    S[j + 1] = w;

                    //VnotS = V.Where(x => x != S[0]).ToArray();
                    foreach (var v in VnotS)
                    {
                        c = Convert.ToInt32(MatrixC.Rows[w].Cells[v].Value);
                        if (c == 0)
                            c = 1000000;

                        if (D[i, w] + c < D[i, v])
                            P[i, v] = w;
                        D[i, v] = Math.Min(D[i, v], (D[i, w] + c));
                    }

                }
            }
            //Конец дейкстры
            for (int i = 0; i < A; i++)
            {
                for (int j = 0; j < B; j++)
                {
                    MatrixD.Rows[i].Cells[j].Value = D[i, j + A];
                }
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void MatrixShort_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
